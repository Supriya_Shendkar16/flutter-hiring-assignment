class Globals {
  static const bool LOG=true;
  static const REPOSITORIES_API = "https://api.github.com/repositories?since=1296269";
  static const REPOSITORIES_DETAIL_API = "https://api.github.com/repos/";

  //Headers
  static const ACCEPT = "accept";
  static const ACCEPT_VALUE = "application/vnd.github.v3+json";

}