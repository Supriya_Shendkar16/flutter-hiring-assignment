import 'package:flutter/material.dart';

final headerStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
);
