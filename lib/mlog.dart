import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'globals.dart';

class HLog {
  static void log(message) {
    if(Globals.LOG) {
      debugPrint("Log: " + message.toString());
    }
  }
}