import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'constant.dart';
import 'globals.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  final String repoFullName;
  final String repoName;

  DetailScreen({Key key, @required this.repoFullName, @required this.repoName})
      : super(key: key);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  var repoDetails;
  var ownerDetails;

  @override
  void initState() {
    super.initState();
    this.getRepoDetails();
    print("" + Globals.REPOSITORIES_DETAIL_API + widget.repoFullName);
  }

  Future<String> getRepoDetails() async {
    try {
      var response = await http.get(
          Uri.encodeFull(Globals.REPOSITORIES_DETAIL_API + widget.repoFullName),
          headers: {"accept": "application/vnd.github.v3+json"});
      if (this.mounted) {
        setState(() {
          repoDetails = json.decode(response.body);
          ownerDetails = repoDetails['owner'];

          print(repoDetails);
        });
      }
    } catch (e) {}
    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.repoName,
          style: TextStyle(fontSize: 15),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 100,
              backgroundImage: NetworkImage(ownerDetails['avatar_url']),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "OwnerName : ",
                  style: headerStyle,
                ),
                Text(ownerDetails['login']),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Text(repoDetails['description']),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Open Issues : ",
                  style: headerStyle,
                ),
                Text(repoDetails['open_issues_count'].toString()),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(
                  Icons.remove_red_eye,
                  size: 20,
                ),
                Text(repoDetails['watchers_count'].toString()),
                SizedBox(
                  width: 20,
                ),
                Icon(
                  Icons.star_border,
                  size: 20,
                ),
                Text(repoDetails['stargazers_count'].toString()),
                SizedBox(
                  width: 20,
                ),
                Icon(
                  FontAwesomeIcons.codeBranch,
                  size: 20,
                ),
                Text(repoDetails['forks_count'].toString()),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
