import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import 'detail_screen.dart';
import 'globals.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Github Repositeries'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List repositeries = List();
  var response;

  @override
  void initState() {
    super.initState();
    this.getRepositeries();
  }

  Future<String> getRepositeries() async {
    try {
      response = await http.get(Uri.encodeFull(Globals.REPOSITORIES_API),
          headers: {"accept": "application/vnd.github.v3+json"});
      if (this.mounted) {
        setState(() {
          repositeries = json.decode(response.body);
        });
      }
    } catch (e) {}
    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Scaffold(
          body: ListView.builder(
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailScreen(
                                repoFullName: repositeries[index]['full_name'],
                                repoName: repositeries[index]['name'],
                              )),
                    );
                  },
                  child: Card(
                    elevation: 5,
                    color: Colors.white,
                    child: Container(
                      padding: EdgeInsets.only(left: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            radius: 60,
                            backgroundImage: NetworkImage(
                              repositeries[index]['owner']['avatar_url'],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "OwnerName:  " +
                                repositeries[index]['owner']['login'],
                            style: TextStyle(
                                fontSize: 15,
                                color: Theme.of(context).accentColor),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Repo Name:  " + repositeries[index]['name'],
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 14, color: Colors.black),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            repositeries[index]['description'],
                            style: TextStyle(fontSize: 13),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: repositeries == null ? 0 : repositeries.length),
        )
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
